const express = require("express");
const app = express();

const PORT = 8070;

app.get("/", (req, res) => {
  res.send("Hello, World!"); // Sending "Hello, World!" as the response
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
